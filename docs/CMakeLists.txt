if (BUILD_DOC)
    find_package(Doxygen)
    if (DOXYGEN_FOUND)
        set(DOXYGEN_INPUT_DIR ${PROJECT_SOURCE_DIR}/src)

        set(DOXYFILE_IN ${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile.in)
        set(DOXYFILE_OUT ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile)

        # Replace CMake variables by their values
        configure_file(${DOXYFILE_IN} ${DOXYFILE_OUT} @ONLY)

        message(STATUS "Building documentation with doxygen")
        add_custom_target(
          doc
          COMMAND ${DOXYGEN_EXECUTABLE} ${DOXYFILE_OUT}
          WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
          COMMENT "Generating API documentation with Doxygen"
          VERBATIM
        )
    endif()
endif()

