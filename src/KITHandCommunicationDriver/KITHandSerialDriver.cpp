#include "KITHandSerialDriver.h"
#include "SerialPort.h"

#define SERIAL_BAUDRATE 115200
#define MAX_SEND_FREQUENCY 50 // in Hz. Any data that is tried to be sent faster will be skipped

namespace KITHand
{
    void KITHandSerialDriver::connect(KITHand::HandDevice &device)
    {
        // Wait until current scan has finished, so that every port should be accessable
        _scanningAllowed = false;
        while (_scanRunning) { std::this_thread::sleep_for(std::chrono::milliseconds(1)); }

        try {
            _currentPort = std::make_unique<SerialPort>(device.serialDeviceFile);

            _readWriteThreadStop = false;
            _readWriteThread = std::thread
            {
                [this]
                {
                    readLoop();
                }
            };

            setConnectionState(State::Connected);
        }
        catch (std::system_error& e)
        {
            LOG_WARN << "Exception while conencting to serial port '" << device.serialDeviceFile << "':\n" << e.what();
            setConnectionState(State::FailedToConnectDevice);
        }
        _scanningAllowed = true;
    }

    void KITHandSerialDriver::disconnect()
    {
        if (_readWriteThread.joinable())
        {
            _readWriteThreadStop = true;
            _readWriteThread.join();
            setConnectionState(State::Disconnected);
        }
    }

    std::future<std::vector<HandDevice>> KITHandSerialDriver::scanForDevices(unsigned int timeoutMS)
    {
        return std::async(std::launch::async, [this, timeoutMS]{ return scan(timeoutMS);});
    }

    void KITHandSerialDriver::sendRaw(const std::vector<char> raw)
    {
        auto diff = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - _timestampLastSendCall);
        if (diff < getMinimalTimeBetweenSendCommands())
        {
            return;
        }

        if (_currentPort && (_currentConnectionState == State::Connected))
        {
            int ret = _currentPort->writeToPort(raw.data(), static_cast<unsigned int>(raw.size()));
            if (ret == -1)
            {
                setConnectionState(State::DeviceLost);
                _readWriteThreadStop = true;
            }
            _timestampLastSendCall = std::chrono::high_resolution_clock::now();

            LOG_DEBUG << "Sending raw: '" << std::string(raw.data(), raw.size()) << "'";
        }
    }

    void KITHandSerialDriver::registerDataAvaiableCallback(std::function<void (const std::vector<char>&)> callback)
    {
        _dataCallback = callback;
    }

    void KITHandSerialDriver::registerConnectionStateChangedCallback(std::function<void (const State)> callback)
    {
        _connectionChangedCallback = callback;
    }

    std::chrono::milliseconds KITHandSerialDriver::getMinimalTimeBetweenSendCommands()
    {
        return std::chrono::milliseconds(1000 / MAX_SEND_FREQUENCY);
    }

    KITHandSerialDriver::~KITHandSerialDriver()
    {
        if (_scanThread.joinable())
        {
            _scanThread.join();
        }

        if (_readWriteThread.joinable())
        {
            _readWriteThreadStop = true;
            _readWriteThread.join();
        }
    }

    void KITHandSerialDriver::readLoop()
    {
        char buffer[200];

        while(!_readWriteThreadStop)
        {
            int bytesReceived = _currentPort->readFromPortImmediatly(buffer);
            if (bytesReceived > 0)
            {
                std::vector<char> data;
                data.reserve(static_cast<size_t>(bytesReceived));
                for (int i = 0; i < bytesReceived; ++i)
                {
                    data.push_back(buffer[i]);
                }
                _dataCallback(data);
            }
            else if (bytesReceived == -1)
            {
                _readWriteThreadStop = true;
                setConnectionState(State::DeviceLost);
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }
        _currentPort.reset();
    }

    SensorProtocol KITHandSerialDriver::detectSensorProtocol(const std::vector<char> &data) const
    {
        const std::vector<char> linefeed{'\n'};

        // First check for binary protocol
        // -- we assume it is a binary protocol, if we find the startSequence and the following byte represents a known protocol-version
        #define startSequenceByte   static_cast<char>(0b10101010)
        const std::vector<char> binaryStartSequence{startSequenceByte,startSequenceByte,startSequenceByte,startSequenceByte};

        auto idx = std::search(data.begin(), data.end(), binaryStartSequence.begin(), binaryStartSequence.end());
        if (idx != data.end())
        {
            // Check the next byte if it is a known version byte
            idx += static_cast<long>(binaryStartSequence.size());
            if (std::distance(idx, data.end()) >= 1)
            {
                if (BinarySensorProtocolToDataLength.count(*idx))
                {
                    LOG_INFO << "Binary detected";
                    return SensorProtocol::Binary;
                }
            }
        }

        // Next try to split the data at linefeeds and try to parse the splitted segments
        std::vector<char> dataBuffer = data;
        std::vector<char>::iterator lastIt = dataBuffer.begin();
        for (
                auto idx = std::search(lastIt, dataBuffer.end(), linefeed.begin(), linefeed.end());
                idx != dataBuffer.end();
                idx = std::search(lastIt, dataBuffer.end(), linefeed.begin(), linefeed.end())
            )
        {
            const std::string line(lastIt, idx);

            if (!Parsing::ParseData_TposTpwmFposFpwm(line).empty())
            {
                LOG_INFO << "TposTpwmFposFpwm detected";
                return SensorProtocol::TposTpwmFposFpwm;
            }
            if (!Parsing::ParseData_TposFposIMUDist(line).empty())
            {
                LOG_INFO << "TposFposIMUDist detected";
                return SensorProtocol::TposFposIMUDist;
            }
            if (!Parsing::ParseData_MxPosPwm_M2(line).empty() || !Parsing::ParseData_MxPosPwm_M3(line).empty())
            {
                LOG_INFO << "MxPosPwm detected";
                return SensorProtocol::MxPosPwm;
            }

            lastIt = idx + 1;
        }

        return SensorProtocol::unknown;
    }

    std::string KITHandSerialDriver::detectMACAddress(const std::vector<char> &data) const
    {
        const std::vector<char> macAdressStartSequence{0x1F, 0x1F, 0x1F};
        auto idx = std::search(data.begin(), data.end(), macAdressStartSequence.begin(), macAdressStartSequence.end());
        if (idx != data.end())
        {
            idx += 3; // Skip macAdressStartSequence
            if (idx < data.end() - 17)
            {
                // next 17 bytes should represent the MAC-address
                return std::string(idx, idx + 17);
            }
        }
        return std::string();
    }

    void KITHandSerialDriver::setConnectionState(State state)
    {
        _currentConnectionState = state;
        _connectionChangedCallback(state);
    }

    std::vector<HandDevice> KITHandSerialDriver::scan(unsigned int timeoutMS)
    {
        std::vector<HandDevice> scanDiscoveredDevices = std::vector<HandDevice>();
        if (!_scanRunning && _scanningAllowed)
        {
            _scanRunning = true;
            std::vector<std::string> portsToCheck {"ttyUSB", "ttyACM0"};
            auto start = std::chrono::high_resolution_clock::now();
            while (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - start).count() < timeoutMS)
            {
                for (const std::string& portType : portsToCheck)
                {
                    std::vector<std::string> availablePorts = SerialPort::getAvailablePortsWithPrefix(portType);
                    for (const std::string& portName : availablePorts)
                    {
                        if (_currentPort && portName == _currentPort->getPortName())
                        {
                            continue;
                        }
                        bool portCheckedAndDeviceFound = false;
                        for (const HandDevice& d : scanDiscoveredDevices)
                        {
                            if (d.serialDeviceFile == portName)
                            {
                                portCheckedAndDeviceFound = true;
                                break;
                            }
                        }
                        if (portCheckedAndDeviceFound)
                        {
                            continue;
                        }

                        try
                        {
                            SerialPort port(portName);

                            // Send command to port to try to get identification response (MAC-address of bluetooth module) from device
                            char sendData[] = {'I','\n'};
                            port.writeToPort(sendData, 2);
                            std::this_thread::sleep_for(std::chrono::microseconds(1000));

                            // Try to read 100 bytes and parse received data
                            char buffer[100];
                            int readBytes = port.readFromPort(buffer, 100, timeoutMS / 2);
                            if (readBytes > 0)
                            {
                                std::vector<char> data(buffer, buffer + readBytes);
                                std::string macAddr = detectMACAddress(data);
                                bool identifiedHandDevice = false;
                                if (!macAddr.empty())
                                {
                                    for (const HandDevice& d : HandInfo)
                                    {
                                        if (d.macAdress == macAddr)
                                        {
                                            HandDevice dc = d;
                                            dc.macAdress = macAddr;
                                            dc.hardwareTarget = HardwareTarget::Serial;
                                            dc.serialDeviceFile = portName;
                                            dc.abilities.bluetoothNameChangeable = false;
                                            scanDiscoveredDevices.push_back(dc);
                                            identifiedHandDevice = true;
                                            break;
                                        }
                                    }
                                }

                                if (!identifiedHandDevice)
                                {
                                    SensorProtocol protocol = detectSensorProtocol(data);
                                    if (protocol != SensorProtocol::unknown)
                                    {
                                        // Setup HandDevice and store it
                                        HandDevice d;
                                        d.name = "";
                                        d.macAdress = "";
                                        d.serialDeviceFile = portName;
                                        d.hardwareTarget = HardwareTarget::Serial;
                                        d.abilities.bluetoothNameChangeable = false;
                                        d.abilities.handOrientation = HandOrientation::unknown;
                                        d.abilities.handSize = "unknown";
                                        d.abilities.sensorProtocol = protocol;
                                        d.abilities.controlProtocol = ControlProtocol::unknown;
                                        d.abilities.receivesAdditionalGraspCommands = false;

                                        scanDiscoveredDevices.push_back(d);
                                    }
                                    else
                                    {
                                        LOG_INFO << "Could not determine sensorProtocol on port: " << portName;
                                    }
                                }
                            }
                            else
                            {
                                LOG_INFO << "Did not receive any bytes on port: " << portName;
                            }
                        }
                        catch (const std::system_error& e)
                        {
                            LOG_WARN << "Exception while checking serial ports: \n" << e.what();
                            continue;
                        }
                    }
                }
                std::this_thread::sleep_for(std::chrono::milliseconds(10));
            }
            _scanRunning = false;
        }

        return scanDiscoveredDevices;
    }
}
