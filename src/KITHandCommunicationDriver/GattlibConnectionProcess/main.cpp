#include "GattlibConnectionProcess.h"
#include <csignal>
#include <iostream>

using namespace  KITHand;

static GattlibConnectionProcess* process;

[[noreturn]] void kill_handler(int s){
    LOG_WARN << "Caught: " << s;
    std::cout << "asdjfäasipfmäaspdofmäapofmäaoemfa" << std::endl;
    delete process;
    exit(1);
}

int main(int argc, char* argv[])
{
    INIT_LOG

    if (argc < 2)
    {
        std::logic_error("GattlibScanProcess expects atleast 1 parameter. Returning!");
        return 0;
    }

    std::signal(SIGTERM, kill_handler);

    process = new GattlibConnectionProcess(std::string(argv[1]));
    delete process;
    return 0;

}
