#include "SharedMemory.h"

using namespace boost::interprocess;

namespace KITHand
{
    SharedMemory::SharedMemory(const std::string &name, Mode mode, bool create) :
        _name(name),
        _mode(mode)
    {
        if (create)
        {
            shared_memory_object::remove(_name.c_str());
            _sharedMemory = managed_shared_memory(create_only, _name.c_str(), 65536);
            _queue = _sharedMemory.construct<shm::RingBuffer>("_queue")();
        }
        else
        {
            _sharedMemory = managed_shared_memory(open_only, _name.c_str());
            _queue = _sharedMemory.find<shm::RingBuffer>("_queue").first;
        }
    }

    SharedMemory::~SharedMemory()
    {
    }

    bool SharedMemory::dataAvailable() const
    {
        return _queue->read_available() > 0;
    }

    std::vector<char> SharedMemory::pop()
    {
        std::vector<char> result;
        if (_mode == Mode::READ)
        {
            shm::vector_alloc ca(_sharedMemory.get_segment_manager());
            shm::sharedVector v(ca);
            if (_queue->pop(v))
            {
                std::copy(v.begin(), v.end(), std::back_inserter<std::vector<char>>(result));
            }
        }
        return result;
    }

    void SharedMemory::push(const std::vector<char> data)
    {
        if (_mode == Mode::WRITE)
        {
            shm::vector_alloc ca(_sharedMemory.get_segment_manager());
            shm::sharedVector v(ca);
            v.resize(data.size());
            std::copy(data.begin(), data.end(),v.begin());
            _queue->push(v);
        }
    }

    void SharedMemory::cleanup()
    {
        static bool cleaned = false;

        if (!cleaned)
        {
            _sharedMemory.destroy_ptr(_queue);
            shared_memory_object::remove(_name.c_str());
            cleaned = true;
        }
    }
}
