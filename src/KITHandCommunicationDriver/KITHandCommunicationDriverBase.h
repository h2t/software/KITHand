#pragma once

#include "HandInfo.h"
#include "ConnectionState.h"
#include "core/Logging.h"
#include <vector>
#include <string>
#include <functional>
#include <future>

namespace KITHand
{
    /**
     * The state of a connection
     */
    using State = ConnectionState::State;

    /**
     * @brief The KITHandCommunicationDriverBase class is the pure virtual base of any specialized driver (e.g. bluetooth or serial driver).
     */
    class KITHandCommunicationDriverBase
    {
    public:
        virtual ~KITHandCommunicationDriverBase() {}

        /**
         * @brief Tries to connect to the hand described by the given \p HandDevice.
         * @param device The describtion of the hand to connect to
         */
        virtual void connect(HandDevice& device) = 0;
        /**
         * @brief Closes the current connection to a hand
         */
        virtual void disconnect() = 0;
        /**
         * @brief Scans asynchronous for available hands until the given timeout is reached
         * @param timeoutMS The timeout in milliseconds
         * @return a future to the list of detected hands
         */
        virtual std::future<std::vector<HandDevice>> scanForDevices(unsigned int timeoutMS) = 0;
        /**
         * @brief Sends the given byte-vector to the conencted HandDevice
         * @param raw The data to send
         */
        virtual void sendRaw(const std::vector<char> raw) = 0;
        /**
         * @brief Registers the given \p callback as the 'DataAvailableCallback'.
         *
         * The given \p callback will be called whenever new data from the connected HandDevice has arrived.
         *
         * @param callback The callback to register
         */
        virtual void registerDataAvaiableCallback(std::function<void(const std::vector<char>&)> callback) = 0;
        /**
         * @brief Registers the given \p callback as the 'ConnectionStateChangedCallback'.
         *
         * The given \p callback will be called whenever the state of the current connection changes
         *
         * @param callback The callback to register
         */
        virtual void registerConnectionStateChangedCallback(std::function<void(const State)> callback) = 0;

        /**
         * @brief Returns the minimal wait time between two send commands.
         *
         * The implementation of a communication driver should skip send commands that are send with a higher frequency
         * to avoid overloading the communication medium.
         *
         * @return the minimal wait time in milliseconds
         */
        virtual std::chrono::milliseconds getMinimalTimeBetweenSendCommands() = 0;
    };
}
