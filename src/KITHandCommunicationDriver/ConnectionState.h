#pragma once

#include <string_view>

namespace KITHand
{
    namespace ConnectionState
    {
        /**
         * @brief Describes the current state of a connection to a hand.
         */
        enum class State
        {
            Connected,

            FailedToFindReadCharacteristic,
            FailedToFindWriteCharacteristic,
            FailedToFindReadWriteCharacteristics,

            FailedToFindDevice,
            FailedToConnectDevice,

            FailedToOpenAdapter,
            FailedToScanAdapter,

            ConnectedButNoSensorValues,

            DeviceLost,

            Disconnected,
            Disconnecting,

            TryingToConnect,

            Unkown
        };

        /**
         * @brief Checks whether the given \p state represents an error state
         * @param state The state to check
         * @return true is \p state represents an error state
         */
        constexpr bool IsError(State state)
        {
            return (
                        state == State::FailedToConnectDevice ||
                        state == State::FailedToFindDevice ||
                        state == State::FailedToFindReadCharacteristic ||
                        state == State::FailedToFindReadWriteCharacteristics ||
                        state == State::FailedToFindWriteCharacteristic ||
                        state == State::FailedToOpenAdapter ||
                        state == State::FailedToScanAdapter ||
                        state == State::ConnectedButNoSensorValues ||
                        state == State::DeviceLost
                   );
        }

        /**
         * @brief Returns the given \p state as a string.
         * @param state The state to stringify
         * @return the stringified \p state
         */
        constexpr std::string_view ToString(State state)
        {
            switch (state)
            {
                case State::Connected: return "Connected";
                case State::FailedToFindReadCharacteristic: return "FailedToFindReadCharacteristic";
                case State::FailedToFindWriteCharacteristic: return "FailedToFindWriteCharacteristic";
                case State::FailedToFindReadWriteCharacteristics: return "FailedToFindReadWriteCharacteristics";
                case State::FailedToFindDevice: return "FailedToFindDevice";
                case State::FailedToConnectDevice: return "FailedToConnectDevice";
                case State::FailedToOpenAdapter: return "FailedToOpenAdapter";
                case State::FailedToScanAdapter: return "FailedToScanAdapter";
                case State::ConnectedButNoSensorValues: return "ConnectedButNoSensorValues";
                case State::DeviceLost: return "DeviceLost";
                case State::Disconnected: return "Disconnected";
                case State::Disconnecting: return "Disconnecting";
                case State::TryingToConnect: return "TryingToConnect";
                case State::Unkown: return "Unkown";
            }
            return std::string_view();
        }
    }
}
