#include "KITHandCommunicationDriver.h"

#include <regex>
#include <iomanip>
#include <sstream>

using namespace KITHand;

KITHandCommunicationDriver::KITHandCommunicationDriver()
{
    INIT_LOG
    Logging::setFatalHook( [=]{ this->disconnect(); });
    try {
        _bluetoothDriver = std::make_unique<KITHandGattlibDriver>();
        _bluetoothDriver->registerDataAvaiableCallback(std::bind(&KITHandCommunicationDriver::dataReceivedCallback, this, std::placeholders::_1));
        _bluetoothDriver->registerConnectionStateChangedCallback(std::bind(&KITHandCommunicationDriver::connectionStateChangedCallback, this, std::placeholders::_1));
    } catch (std::runtime_error e) {
        LOG_WARN << e.what();
    }

    _serialDriver = std::make_unique<KITHandSerialDriver>();
    _serialDriver->registerDataAvaiableCallback(std::bind(&KITHandCommunicationDriver::dataReceivedCallback, this, std::placeholders::_1));
    _serialDriver->registerConnectionStateChangedCallback(std::bind(&KITHandCommunicationDriver::connectionStateChangedCallback, this, std::placeholders::_1));

    _currentConnection = std::make_unique<Connection>();
    _currentConnection->state = State::Unkown;
}

bool KITHandCommunicationDriver::connect(HandDevice& device)
{
    if (device.hardwareTarget == HardwareTarget::Bluetooth && _bluetoothDriver)
    {
        if (device.macAdress.empty())
        {
            return false;
        }

        // connect via KITHandGattlibDriver
        _currentConnection->device = device;
        _bluetoothDriver->connect(device);

        return connected();
    }
    else if (device.hardwareTarget == HardwareTarget::Serial)
    {
        if (device.serialDeviceFile.empty())
        {
            return false;
        }

        // connect via KITHandSerialDriver
        _currentConnection->device = device;
        _serialDriver->connect(device);

        return connected();
    }
    else
    {
        throw std::logic_error {"unhandled device target"};
    }
}

void KITHandCommunicationDriver::disconnect()
{
    if (connected())
    {
        if (_currentConnection->device.hardwareTarget == HardwareTarget::Bluetooth && _bluetoothDriver)
        {
            // disconnect via KITHandGattlibDriver
            _bluetoothDriver->disconnect();
        }
        else if (_currentConnection->device.hardwareTarget == HardwareTarget::Serial)
        {
            // disconnect via KITHandSerialDriver
            _serialDriver->disconnect();
        }
        else
        {
            throw std::logic_error {"unhandled device target"};
        }
    }
}

bool KITHandCommunicationDriver::reconnect()
{
    if (_currentConnection && _currentConnection->state == State::Disconnected)
    {
        return connect(_currentConnection->device);
    }
    return connected();
}

bool KITHandCommunicationDriver::connected() const
{
    if (_currentConnection)
    {
        return _currentConnection->state == State::Connected;
    }
    else
    {
        return false;
    }
}

State KITHandCommunicationDriver::getCurrentConnectionState() const
{
    if (_currentConnection)
    {
        return _currentConnection->state;
    }
    else
    {
        return State::Unkown;
    }
}

HandDevice* KITHandCommunicationDriver::getCurrentConnectedDevice() const
{
    if (_currentConnection)
    {
        return &(_currentConnection->device);
    }
    else
    {
        return nullptr;
    }
}

void KITHandCommunicationDriver::registerConnectionStateChangedCallback(std::function<void (const State)> callback)
{
    _connectionChangedCallback = callback;
}

std::vector<HandDevice> KITHandCommunicationDriver::scanForDevices(KITHandCommunicationDriver::ScanMode mode, unsigned int scanDuration) const
{
    std::future<std::vector<HandDevice>> future = scanForDevicesAsync(mode, scanDuration);
    future.wait();
    return future.get();
}

std::future<std::vector<HandDevice>> KITHandCommunicationDriver::scanForDevicesAsync(ScanMode mode, unsigned int scanDuration) const
{
    return std::async(std::launch::async, [this, mode, scanDuration]{
        std::future<std::vector<HandDevice>> futureSerial;
        std::future<std::vector<HandDevice>> futureBluetooth;
        if (mode == ScanMode::Serial || mode == ScanMode::Both)
        {
            futureSerial = _serialDriver->scanForDevices(scanDuration);
        }
        if (mode == ScanMode::Bluetooth || mode == ScanMode::Both)
        {
            if (_bluetoothDriver)
            {
                futureBluetooth = _bluetoothDriver->scanForDevices(scanDuration);
            }
        }
        std::vector<HandDevice> result;
        if (futureSerial.valid())
        {
            futureSerial.wait();
            auto resultSerial = futureSerial.get();
            result.insert(result.end(), resultSerial.begin(), resultSerial.end());
        }
        if (futureBluetooth.valid())
        {
            futureBluetooth.wait();
            auto resultBluetooth = futureBluetooth.get();
            result.insert(result.end(), resultBluetooth.begin(), resultBluetooth.end());
        }
        return result;
    });
}

void KITHandCommunicationDriver::waitForCommunicationMedium() const
{
    if (_currentConnection)
    {
        if (_currentConnection->device.hardwareTarget == HardwareTarget::Bluetooth)
        {
            std::this_thread::sleep_for(_bluetoothDriver->getMinimalTimeBetweenSendCommands());
        }
        else if (_currentConnection->device.hardwareTarget == HardwareTarget::Serial)
        {
            std::this_thread::sleep_for(_serialDriver->getMinimalTimeBetweenSendCommands());
        }
    }
}

int64_t KITHandCommunicationDriver::getThumbPWM() const
{
    return _thumbPWM;
}

int64_t KITHandCommunicationDriver::getThumbPos() const
{
    return _thumbPos;
}

int64_t KITHandCommunicationDriver::getFingersPWM() const
{
    return _fingersPWM;
}

int64_t KITHandCommunicationDriver::getFingersPos() const
{
    return _fingersPos;
}

int64_t KITHandCommunicationDriver::getIMURoll() const
{
    return _IMUroll;
}

int64_t KITHandCommunicationDriver::getIMUPitch() const
{
    return _IMUpitch;
}

int64_t KITHandCommunicationDriver::getIMUYaw() const
{
    return _IMUyaw;
}

int64_t KITHandCommunicationDriver::getTimeOfFlight() const
{
    return _timeOfFlight;
}

void KITHandCommunicationDriver::sendRaw(std::string raw) const
{
    if (!raw.empty())
    {
        sendRaw(std::vector<char>(raw.begin(), raw.end()));
    }
}

void KITHandCommunicationDriver::sendRaw(std::vector<char> raw) const
{
    if (connected() && !raw.empty())
    {
        if (raw.back() != '\n')
        {
            raw.push_back('\n');
        }

        if (_currentConnection->device.hardwareTarget == HardwareTarget::Bluetooth && _bluetoothDriver)
        {
            // sendRaw via KITHandGattlibDriver
            _bluetoothDriver->sendRaw(raw);
        }
        else if (_currentConnection->device.hardwareTarget == HardwareTarget::Serial)
        {
            // sendRaw via KITHandSerialDriver
            _serialDriver->sendRaw(raw);
        }
        else
        {
            throw std::logic_error {"unhandled device target"};
        }
    }
}

void KITHandCommunicationDriver::sendGrasp(uint64_t g) const
{
    if (connected())
    {
        if (g > ControlOptions::maxGraspId)
        {
            throw std::invalid_argument
            {
                "KITHandCommunicationDriver::send_grasp( g = " + std::to_string(g) +
                        " ): the maximal value for g is " + std::to_string(ControlOptions::maxGraspId)
            };
        }
        sendRaw("g" + std::to_string(g) + '\n');
    }
}

void KITHandCommunicationDriver::sendThumbPosition(uint64_t pos, uint64_t v, uint64_t pwm) const
{
    if (connected())
    {
        static const std::string name = "send thumb PWM";
        sendPWM(v, pwm, pos, ControlOptions::maxPosThumb, name, 2);
    }
}

void KITHandCommunicationDriver::sendFingersPosition(uint64_t pos, uint64_t v, uint64_t pwm) const
{
    if (connected())
    {
        static const std::string name = "send fingers PWM";
        sendPWM(v, pwm, pos, ControlOptions::maxPosFingers, name, 3);
    }
}

void KITHandCommunicationDriver::sendPWM(uint64_t v, uint64_t pwm, uint64_t pos, uint64_t max_pos, const std::string &name, uint64_t motor) const
{
    if (connected())
    {
        if (v < ControlOptions::minVelocity || v > ControlOptions::maxVelocity)
        {
            throw std::invalid_argument
                {
                    name + "( v = " + std::to_string(v) +
                    " , pwm = " + std::to_string(pwm) +
                    " , pos = " + std::to_string(pos) +
                    " ): The interval for v is [" + std::to_string(ControlOptions::minVelocity) +
                    ", " + std::to_string(ControlOptions::maxVelocity) + "]"
                };
        }
        if (pwm > ControlOptions::maxPWM)
        {
            throw std::invalid_argument
                {
                    name + "( v = " + std::to_string(v) +
                    " , pwm = " + std::to_string(pwm) +
                    " , pos = " + std::to_string(pos) +
                    " ): The interval for pwm is [0, , " +
                    std::to_string(ControlOptions::maxPWM) + "]"
                };
        }
        if (pos > max_pos)
        {
            throw std::invalid_argument
                {
                    name + "( v = " + std::to_string(v) +
                    " , pwm = " + std::to_string(pwm) +
                    " , pos = " + std::to_string(pos) +
                    " ): The interval for pos is [0, " +
                    std::to_string(max_pos) + "]"
                };
        }
        std::stringstream str;
        // TODO --- needs to be done better, whole protocol handling should be done by seperate classes for each protocol version
        if (_currentConnection->device.abilities.controlProtocol == ControlProtocol::MX_Velocity_MaxPWM_Position)
        {
            str << 'M' << motor << ',' << v << ',' << pwm << ',' << pos << '\n';
        }
        else if (_currentConnection->device.abilities.controlProtocol == ControlProtocol::Ptf_Position_Velocity_ClosingPWM_tf ||
                 _currentConnection->device.abilities.controlProtocol == ControlProtocol::unknown)
        {
            str << 'P' << ((motor == 2) ? 't' : 'f') << pos << ',' << v << ',' << pwm << ',' << 'f' << '\n';
        }

        sendRaw(str.str());
    }
}

void KITHandCommunicationDriver::sendPseudoATCommand(const char &command, const std::string input) const
{
    if (connected() && _currentConnection->device.hardwareTarget == HardwareTarget::Bluetooth)
    {
        LOG_DEBUG << "sendATCommand -- command: " << command << " input: " << input << '\n';

        char startChar = 0x1F;
        std::string s;
        // Start sequence to indicate this is a pseudo AT command
        s.push_back(startChar);
        s.push_back(startChar);
        s.push_back(startChar);
        // Command byte
        s.push_back(command);
        if (!input.empty())
        {
            // Input length
            s.push_back(static_cast<char>(input.size()));
            // Input
            s.append(input);
            // Newline to indicate end of command
            s.push_back('\n');
        }
        this->sendRaw(s);
    }
}

void KITHandCommunicationDriver::openHand() const
{
    sendGrasp(0);
}

void KITHandCommunicationDriver::closeHand() const
{
    sendGrasp(1);
}

void KITHandCommunicationDriver::dataReceivedCallback(const std::vector<char>& data)
{
    parseSensorValue(data);
}

void KITHandCommunicationDriver::connectionStateChangedCallback(const State newState)
{
    _currentConnection->state = newState;
    if (_connectionChangedCallback)
    {
        _connectionChangedCallback(newState);
    }
}

void KITHandCommunicationDriver::parseSensorValueLine(const std::string& line)
{
    const auto print_unknown = [&](const char* protocolname)
    {
        std::stringstream s;
        s << std::hex << std::setw(2) << std::setfill('0');
        for(char c : line)
        {
            s << std::hex << static_cast<std::int16_t>(c) << ' ';
        }

        LOG_WARN << "[protocol " << protocolname << "] unknown sensor data '" << line << "' (len = " << line.size() << ", hex = " << s.str() << ")\n";
    };

    if(_currentConnection->device.abilities.sensorProtocol == SensorProtocol::TposTpwmFposFpwm)
    {
        std::vector<long long> values = Parsing::ParseData_TposTpwmFposFpwm(line);
        if (values.size() == 4)
        {
            _thumbPos = values[0];
            _thumbPWM = values[1];
            _fingersPos = values[2];
            _fingersPWM = values[3];
        }
        else
        {
            print_unknown("TposTpwmFposFpwm");
        }
    }
    else if(_currentConnection->device.abilities.sensorProtocol == SensorProtocol::TposFposIMUDist)
    {
        std::vector<long long> values = Parsing::ParseData_TposFposIMUDist(line);
        if (values.size() == 6)
        {
            _thumbPos = values[0];
            _fingersPos = values[1];
            _IMUroll = values[2];
            _IMUpitch = values[3];
            _IMUyaw = values[4];
            _timeOfFlight = values[5];
        }
        else
        {
            print_unknown("TposFposIMUDist");
        }
    }
    else if(_currentConnection->device.abilities.sensorProtocol == SensorProtocol::MxPosPwm)
    {
        std::vector<long long> values = Parsing::ParseData_MxPosPwm_M2(line);
        if (values.size() == 2)
        {
            _thumbPos = values[0];
            _thumbPWM = values[1];
            return;
        }
        values = Parsing::ParseData_MxPosPwm_M3(line);
        if (values.size() == 2)
        {
            _fingersPos = values[0];
            _fingersPWM = values[1];
            return;
        }
        print_unknown("MxPosPwm");
    }
    else
    {
        throw std::logic_error {"unhandled protocol version"};
    }
}

void KITHandCommunicationDriver::parseSensorValueBinary(const std::vector<char>& line, char version)
{
    {
        std::stringstream s;
        s << std::hex << std::setw(2) << std::setfill('0');
        for(char c : line)
        {
            s << std::hex << static_cast<std::int16_t>(c) << ' ';
        }
        LOG_WARN << "[protocol binary] unknown sensor data '" << std::string(line.begin(), line.end()) << "' (len = " << line.size() << ", hex = " << s.str() << ")\n";
    }

    // get a variable buffer to build the data into
    long long buffer{0};

    if (version == 0x05)
    {
        // position of finger motor
        buffer = line[0] << 24;
        buffer |= line[1] << 16;
        buffer |= line[2] << 8;
        buffer |= line[3];

        _fingersPos = buffer;

        // PWM of finger motor
        buffer = line[4] << 24;
        buffer |= line[5] << 16;
        buffer |= line[6] << 8;
        buffer |= line[7];

        _fingersPWM = buffer;

        // position of thumb motor
        buffer = line[8] << 24;
        buffer |= line[9] << 16;
        buffer |= line[10] << 8;
        buffer |= line[11];

        _thumbPos = buffer;

        // PWM of thumb motor
        buffer = line[12] << 24;
        buffer |= line[13] << 16;
        buffer |= line[14] << 8;
        buffer |= line[15];

        _thumbPWM = buffer;

        // IMU roll angle
        buffer = line[16] << 8;
        buffer |= line[17];

        _IMUroll = buffer;

        // IMU pitch angle
        buffer = line[18] << 8;
        buffer |= line[19];

        _IMUpitch = buffer;

        // IMU yaw angle
        buffer = line[20] << 8;
        buffer |= line[21];

        _IMUyaw = buffer;

        // distance sensor
        buffer = line[22] << 8;
        buffer |= line[23];

        _timeOfFlight = buffer;
    }
}

void KITHandCommunicationDriver::parseSensorValue(const std::vector<char>& data)
{
    #define startSequenceByte   static_cast<char>(0b10101010)
    const std::vector<char> binaryStartSequence{startSequenceByte,startSequenceByte,startSequenceByte,startSequenceByte};
    const std::vector<char> linefeed{'\n'};

    _processBuffer.insert(_processBuffer.end(), data.begin(), data.end());
    std::vector<char>::iterator lastIt = _processBuffer.begin();

    std::stringstream s;
    s << std::hex << std::setw(2) << std::setfill('0');
    for(char c : data)
    {
        s << std::hex << static_cast<std::int16_t>(c) << ' ';
    }

    switch(_currentConnection->device.abilities.sensorProtocol)
    {
        case SensorProtocol::TposTpwmFposFpwm:
        case SensorProtocol::TposFposIMUDist:
        case SensorProtocol::MxPosPwm:
            for (
                    auto idx = std::search(lastIt, _processBuffer.end(), linefeed.begin(), linefeed.end());
                    idx != _processBuffer.end();
                    idx = std::search(lastIt, _processBuffer.end(), linefeed.begin(), linefeed.end())
                )
            {
                const std::string line(lastIt, idx);
                parseSensorValueLine(line);
                lastIt = idx + 1;
            }
            _processBuffer.erase(_processBuffer.begin(), lastIt);
            break;

        case SensorProtocol::Binary:

            for (
                 auto idx = std::search(lastIt, _processBuffer.end(), binaryStartSequence.begin(), binaryStartSequence.end());
                 idx != _processBuffer.end();
                 idx = std::search(lastIt, _processBuffer.end(), binaryStartSequence.begin(), binaryStartSequence.end())
            )
            {
                idx += static_cast<long>(binaryStartSequence.size()); // skip startSequence
                if (std::distance(idx, _processBuffer.end()) >= 1) // Atleast one byte after startSequence exisits in buffer
                {
                    // read next char to get version of binary protocol
                    const char version = *idx;
                    ++idx; // skip version-byte
                    if (BinarySensorProtocolToDataLength.count(version))
                    {
                        int dataLength = BinarySensorProtocolToDataLength.at(version);
                        if (std::distance(idx, _processBuffer.end()) >= dataLength)
                        {
                            lastIt = idx + dataLength;
                            const std::vector<char> data(idx, lastIt);
                            parseSensorValueBinary(data, version);
                        }
                        else
                        {
                            break;
                        }
                    }
                    else
                    {
                        throw std::logic_error {"Unhandled binary protocol version ('" + std::string(1, version) + "')"};
                    }
                }
                else
                {
                    break;
                }
            }
            _processBuffer.erase(_processBuffer.begin(), lastIt);
            break;
        default:
            throw std::logic_error {"unhandled protocol version"};
    }

#undef startSequenceByte
}
