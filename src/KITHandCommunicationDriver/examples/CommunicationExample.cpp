#include "KITHandCommunicationDriver.h"
#include "external/InputParser.h"

using namespace KITHand;
int main(int argc, char* argv[])
{
    INIT_LOG

    /*
     * Parse commandline parameters
     */
    KITHandCommunicationDriver::ScanMode mode;
    InputParser::InputParser input(argc, argv);
    const std::string& modeString = input.getCmdOption("--scanmode");
    if (modeString == "both")
    {
        mode = KITHandCommunicationDriver::ScanMode::Both;
    }
    else if (modeString == "bluetooth")
    {
        mode = KITHandCommunicationDriver::ScanMode::Bluetooth;
    }
    else if (modeString == "serial")
    {
        mode = KITHandCommunicationDriver::ScanMode::Serial;
    }
    else
    {
        LOG_INFO << "Unknown mode: '" << modeString << "'. Falling back to mode 'both'";
        mode = KITHandCommunicationDriver::ScanMode::Both;
    }

    /*
     * Initialization of KITHandCommunicationDriver.
     * It is advised to use this class for both serial and bluetooth communication
     * since it already includes correct parsing of incoming data and correct syntax
     * for outgoing data, regardless which hand is connected.
     */
    KITHandCommunicationDriver driver;

    /*
     * Scan for available hands
     */
    std::vector<HandDevice> foundDevices = driver.scanForDevices(mode, 3000);

    // We connect to the first hand that has been found
    if (foundDevices.empty())
    {
        LOG_INFO << "No hand has been found.";
        return 0;
    }
    HandDevice device = foundDevices[0];

    /*
     * Try to connect to device (call waits until connection was either successfull or failed)
     */
    if (!driver.connect(device))
    {
        if (device.hardwareTarget == HardwareTarget::Bluetooth)
        {
            LOG_INFO << "Connecting via bluetooth to hand with mac-address '" << device.macAdress << "' failed.";
        }
        else if (device.hardwareTarget == HardwareTarget::Serial)
        {
            LOG_INFO << "Connecting via serial to hand on serial-port '" << device.serialDeviceFile << "' failed.";
        }
        return 0;
    }

    /*
     * Close the hand
     */
    driver.closeHand();
    /*
     * Wait until it can be assumed that the hand is closed.
     * We only wait until 90% of the maximal motor-positions, since it cannot be guarnateed that the motors always close exactly to their maximal position
     */
    while (driver.getFingersPos() < ControlOptions::maxPosFingers * 0.9 && driver.getThumbPos() < ControlOptions::maxPosThumb * 0.9)
    {
        LOG_INFO << "Waiting for the hand to close...";
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }

    /*
     * Open the hand
     */
    driver.openHand();

    /*
     * Wait 2 sec before sending a specific velocity, maximal PWM and position for the fingers
     */
    std::this_thread::sleep_for(std::chrono::seconds(3));
    driver.sendFingersPosition(ControlOptions::maxPosFingers / 2, ControlOptions::maxVelocity / 2, ControlOptions::maxPWM);

    /*
     * Wait again and open the hand again
     */
    std::this_thread::sleep_for(std::chrono::seconds(3));
    driver.openHand();
    std::this_thread::sleep_for(std::chrono::seconds(3));

    /*
     * Disconnect from the hand
     */
    driver.disconnect();
}
