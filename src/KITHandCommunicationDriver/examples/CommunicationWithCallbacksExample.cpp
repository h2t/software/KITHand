#include "KITHandCommunicationDriver.h"
#include "external/InputParser.h"

using namespace KITHand;

static std::thread _pollingThread;
static std::atomic_bool _pollingThreadStop {false};
static KITHandCommunicationDriverPtr _driver;

void pollingLoop()
{
    while (!_pollingThreadStop)
    {
        LOG_INFO << _driver->getFingersPos();
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
    LOG_INFO << "Exiting polling thread.";
}

void connectionStateChangedCallback(const State state)
{
    LOG_INFO << "ConnectionState changed. New state: " << ConnectionState::ToString(state);

    if (ConnectionState::IsError(state))
    {
        LOG_WARN << "An error in the connection occured. The example will end now.";
    }
    else if (state == State::Connected)
    {
        LOG_INFO << "Now that the connection has been established, we can start polling sensor values.";
        _pollingThread = std::thread
        {
            []
            {
                pollingLoop();
            }
        };
    }
    else if (state == State::Disconnected)
    {
        LOG_INFO << "The connection has been terminated. --> Stop polling of sensor values and end example.";
        if (_pollingThread.joinable())
        {
            _pollingThreadStop = true;
            _pollingThread.join();
        }
    }
}

int main(int argc, char* argv[])
{
    INIT_LOG

    /*
     * Parse commandline parameters
     */
    KITHandCommunicationDriver::ScanMode mode;
    InputParser::InputParser input(argc, argv);
    const std::string& modeString = input.getCmdOption("--scanmode");
    if (modeString == "both")
    {
        mode = KITHandCommunicationDriver::ScanMode::Both;
    }
    else if (modeString == "bluetooth")
    {
        mode = KITHandCommunicationDriver::ScanMode::Bluetooth;
    }
    else if (modeString == "serial")
    {
        mode = KITHandCommunicationDriver::ScanMode::Serial;
    }
    else
    {
        LOG_INFO << "Unknown mode: '" << modeString << "'. Falling back to mode 'both'";
        mode = KITHandCommunicationDriver::ScanMode::Both;
    }

    /*
     * Initialization of KITHandCommunicationDriver.
     * It is advised to use this class for both serial and bluetooth communication
     * since it already includes correct parsing of incoming data and correct syntax
     * for outgoing data, regardless which hand is connected.
     */
    _driver = std::make_unique<KITHandCommunicationDriver>();

    /*
     * Register callback
     */
    _driver->registerConnectionStateChangedCallback(std::bind(&connectionStateChangedCallback, std::placeholders::_1));

    /*
     * Scan for available hands
     */
    std::vector<HandDevice> foundDevices = _driver->scanForDevices(mode, 3000);

    // We connect to the first hand that has been found
    if (foundDevices.empty())
    {
        LOG_INFO << "No hand has been found.";
        return 0;
    }
    HandDevice device = foundDevices[0];

    /*
     * Try to connect to device (call waits until connection was either successfull or failed)
     */
    if (!_driver->connect(device))
    {
        if (device.hardwareTarget == HardwareTarget::Bluetooth)
        {
            LOG_INFO << "Connecting via bluetooth to hand with mac-address '" << device.macAdress << "' failed.";
        }
        else if (device.hardwareTarget == HardwareTarget::Serial)
        {
            LOG_INFO << "Connecting via serial to hand on serial-port '" << device.serialDeviceFile << "' failed.";
        }
        return 0;
    }

    /*
     * Close the hand
     */
    _driver->closeHand();

    /*
     * Wait some time. During that time the pollingThread will output the current finger position
     */
    std::this_thread::sleep_for(std::chrono::seconds(5));


    /*
     * Open the hand
     */
    _driver->openHand();

    std::this_thread::sleep_for(std::chrono::seconds(5));

    /*
     * Disconnect from the hand
     */
    _driver->disconnect();
}
