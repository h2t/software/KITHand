#pragma once

#include "g3log/g3log.hpp"

namespace KITHand
{
#define LOG_INFO LOG(INFO)
#define LOG_WARN LOG(WARNING)
#define LOG_FATAL LOG(FATAL)
#define LOG_DEBUG LOG(DEBUG)

    /**
     * @brief Small wrapper around the g3log-library
     */
    class Logging
    {
    public:
        Logging() = delete;
        ~Logging() = delete;
        /**
         * @brief Initializes the logging for an exectuable.
         *
         * This method must be called once before the very first LOG-call.
         */
        static void initialize();

        /**
         * @brief Sets the hook to be called by the fatal signal handler before flushing the log.
         *
         * @param fatalHook the function to be called
         */
        static void setFatalHook(std::function<void(void)> fatalHook);
    };

#define INIT_LOG Logging::initialize();
}
