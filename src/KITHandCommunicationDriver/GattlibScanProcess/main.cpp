#include "GattlibScanProcess.h"
#include "core/Logging.h"

using namespace KITHand;

int main(int argc, char* argv[])
{
    INIT_LOG

    if (argc < 2)
    {
        std::logic_error("GattlibScanProcess expects atleast 1 parameter. Returning!");
        return 0;
    }

    std::string arg = argv[1];
    unsigned int timeoutMS = 1000;
    try {
        std::size_t pos;
        timeoutMS = static_cast<unsigned int>(std::stoi(arg, &pos));
        if (pos < arg.size()) {
            LOG_WARN << "Trailing characters after number: " << arg;
        }
    } catch (std::invalid_argument const &ex) {
        LOG_WARN << "Invalid number: " << arg;
        return 1;
    } catch (std::out_of_range const &ex) {
        LOG_WARN << "Number out of range: " << arg;
        return 1;
    }

    GattlibScanProcess::scan(timeoutMS);
    return 0;
}
