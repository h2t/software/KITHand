#pragma once
#include <QPushButton>

/**
 * @brief This class represents a QPushButton whose Icon is resized everytime the button itself is resized.
 */
class QPushButtonWithIconResize : public QPushButton
{
private:
    QPixmap _qpSource; //preserve the original, so multiple resize events won't break the quality
    QPixmap _qpCurrent;

    void _displayImage();
public:
    /**
     * @brief Constructs a new QPictureLabel with the given widget as a parent
     * @param aParent The parent of this instance
     */
    QPushButtonWithIconResize(QWidget *aParent) : QPushButton(aParent) { }

    /**
     * @param aPicture The QPixmap for this instance
     */
    void setPixmap(QPixmap aPicture);


    void paintEvent(QPaintEvent *aEvent);
};
