#pragma once

#include <QWidget>
#include <QThread>

#include "QPictureLabel.h"
#include "KITHandCommunicationDriver.h"
#include "AdvancedWidgetController.h"

#ifdef KITHandAdvancedWidget_WITH_3D_VISUALIZATION
#include "Hand3DViewerWidget.h"
#endif

namespace Ui
{
    class KITHandAdvancedWidget;
}

namespace KITHand
{
    /**
     * @brief This class represents the advanced GUI for the KITHandGUI
     *
     * This class is only responsible for the apperance of the GUI itself. Any controlling is done in KITHand::AdvancedWidgetController
     */
    class KITHandAdvancedWidget : public QWidget
    {
        Q_OBJECT

    public:
        /**
         * @brief Constructs a new KITHandAdvancedWidget, connects signals and slots with the underlying controller and starts the control-loop
         * @param parent The parent of this QWidget
         */
        explicit KITHandAdvancedWidget(QWidget *parent = nullptr);
        ~KITHandAdvancedWidget() override;

    private slots:
        void updateScanResults(QStringList names);
        void connectToDevice();
        void disconnectFromDevice();
        void updateConnectPushButtons(bool connectingAllowed);
        void updateConnectionStateLabel(QString stateString);
        void toggleAdvancedWidget();
        void toggleMoreGraspsWidget();
        void toggleGuiElements(bool toggle);

        void togglePWMValues(bool toggle);
        void toggleIMUValues(bool toggle);
        void toggleDistanceValues(bool toggle);
        void toggleChangeNameButton(bool toggle);
        void toggleMoreGraspsButton(bool toggle);

        void sendRawButtonReleased();
        void changeNameButtonReleased();

        void updatePos(long fingersPos, long thumbPos);
        void updatePWM(long fingersPWM, long thumbPWM);
        void updateIMU(long roll, long pitch, long yaw);
        void updateDistance(long distance);

        void onConnectingFinished();
        void onDisconnectingFinished();

        void updateDials();

    private:
        void setupVisualizationWidget();
        void setupAdvancedManualControl();

        Ui::KITHandAdvancedWidget *ui;

        AdvancedWidgetController* _controller;
        QThread* _controllerThread;

#ifdef KITHandAdvancedWidget_WITH_3D_VISUALIZATION
        Hand3DViewerWidget* _viewerWidget;
#endif
    };
}
