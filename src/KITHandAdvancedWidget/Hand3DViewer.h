#pragma once

#include <Inventor/nodes/SoPerspectiveCamera.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#include <Inventor/Qt/SoQt.h>

namespace KITHand {

    /**
     * @brief A 3DViewer based on SoQtExaminerViewer
     */
    class Hand3DViewer : public SoQtExaminerViewer
    {
    public:
        /**
         * @brief Constructs a new Hand3DViewer with the given \p parent
         *
         * Sets up the scene, the camera and the root node of the scene.
         *
         * @param parent The parent of this QWidget
         */
        Hand3DViewer(QWidget* parent = nullptr);
        ~Hand3DViewer() override;
        /**
         * @return a pointer to the root node of the scene
         */
        SoSeparator* getRootNode() const;
        /**
         * @brief Moves the camera of the scene in a way that every object in the scene is visible.
         */
        void cameraViewAll();
    private:
        SbBool processSoEvent(const SoEvent* const event) override;
        SoSeparator* sceneRootNode;
        SoSeparator* contentRootNode;
        SoPerspectiveCamera* camera;
    };
}
