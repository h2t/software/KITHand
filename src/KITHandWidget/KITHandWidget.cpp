#include "KITHandWidget.h"
#include "ui_KITHandWidget.h"

namespace KITHand
{
    KITHandWidget::KITHandWidget(QWidget* parent) :
        QWidget(parent),
        ui(new Ui::KITHandWidget)
    {
        ui->setupUi(this);
    #ifdef KITHandWidget_WITH_ICE
        _server.overrideLowlevelDriver(_driver);
    #else
        ui->groupBoxIce->setHidden(true);
        ui->groupBoxIce->setDisabled(true);
    #endif

        connect(ui->pushButtonSendRaw,       SIGNAL(released()), this, SLOT(on_pushButtonSendRaw_released()));
        connect(ui->pushButtonConnect,       SIGNAL(released()), this, SLOT(on_pushButtonConnect_released()));
        connect(ui->pushButtonSendGrasp,     SIGNAL(released()), this, SLOT(on_pushButtonSendGrasp_released()));
        connect(ui->pushButtonSendThumb,     SIGNAL(released()), this, SLOT(on_pushButtonSendThumb_released()));
        connect(ui->pushButtonIceConnect,    SIGNAL(released()), this, SLOT(on_pushButtonIceConnect_released()));
        connect(ui->pushButtonDisconnect,    SIGNAL(released()), this, SLOT(on_pushButtonDisconnect_released()));
        connect(ui->pushButtonSendFingers,   SIGNAL(released()), this, SLOT(on_pushButtonSendFingers_released()));
        connect(ui->pushButtonIceDisconnect, SIGNAL(released()), this, SLOT(on_pushButtonIceDisconnect_released()));

        //limits
        ui->spinBoxThumbPWM->setMaximum(static_cast<int>(ControlOptions::maxPWM));
        ui->spinBoxThumbPWM->setValue(static_cast<int>(ControlOptions::maxPWM) / 2);

        ui->spinBoxThumbPos->setMaximum(static_cast<int>(ControlOptions::maxPosThumb));
        ui->spinBoxThumbPos->setValue(static_cast<int>(ControlOptions::maxPosThumb) / 2);
        ui->spinBoxThumbPos->setSingleStep(1000);

        ui->spinBoxThumbVel->setMinimum(static_cast<int>(ControlOptions::minVelocity));
        ui->spinBoxThumbVel->setMaximum(static_cast<int>(ControlOptions::maxVelocity));
        ui->spinBoxThumbVel->setValue(static_cast<int>((ControlOptions::maxVelocity - ControlOptions::minVelocity) / 2 + ControlOptions::minVelocity));

        ui->spinBoxFingersPWM->setMaximum(static_cast<int>(ControlOptions::maxPWM));
        ui->spinBoxFingersPWM->setValue(static_cast<int>(ControlOptions::maxPWM) / 2);

        ui->spinBoxFingersPos->setMaximum(static_cast<int>(ControlOptions::maxPosFingers));
        ui->spinBoxFingersPos->setValue(static_cast<int>(ControlOptions::maxPosFingers) / 2);
        ui->spinBoxFingersPos->setSingleStep(1000);

        ui->spinBoxFingersVel->setMinimum(static_cast<int>(ControlOptions::minVelocity));
        ui->spinBoxFingersVel->setMaximum(static_cast<int>(ControlOptions::maxVelocity));
        ui->spinBoxFingersVel->setValue(static_cast<int>((ControlOptions::maxVelocity - ControlOptions::minVelocity) / 2 + ControlOptions::minVelocity));

        startTimer(100);
    }

    KITHandWidget::~KITHandWidget()
    {
#ifdef KITHandWidget_WITH_ICE
        _server.stopIceServer();
#endif
        _driver.disconnect();
        delete ui;
    }

    void KITHandWidget::timerEvent(QTimerEvent*)
    {
        ui->labelThumbPWM->setText(QString::number(_driver.getThumbPWM()));
        ui->labelThumbPos->setText(QString::number(_driver.getThumbPos()));
        ui->labelFingersPWM->setText(QString::number(_driver.getFingersPWM()));
        ui->labelFingersPos->setText(QString::number(_driver.getFingersPos()));
        ui->labelIMURoll->setText(QString::number(_driver.getIMURoll()));
        ui->labelIMUPitch->setText(QString::number(_driver.getIMUPitch()));
        ui->labelIMUYaw->setText(QString::number(_driver.getIMUYaw()));
        ui->labelTimeOfFlight->setText(QString::number(_driver.getTimeOfFlight()));
    }

    void KITHandWidget::on_pushButtonConnect_released()
    {
        const std::string addr = ui->comboBoxMacAddress->currentText().trimmed().toStdString().substr(0, 17);
        const auto p = static_cast<SensorProtocol>(ui->comboBoxProtocol->currentIndex());

        HandDevice d;
        d.hardwareTarget = HardwareTarget::Bluetooth;
        d.macAdress = addr;
        d.abilities.sensorProtocol = p;
        bool connected = _driver.connect(d);

        ConnectionState::State r = _driver.getCurrentConnectionState();
        ui->labelStatus->setText(QString::fromStdString(std::string
        {
            ConnectionState::ToString(r)
        }));

        if (connected)
        {
            ui->pushButtonConnect->setDisabled(true);
            ui->pushButtonDisconnect->setEnabled(true);
            ui->widgetSensors->setEnabled(true);
            ui->widgetControl->setEnabled(true);

            ui->comboBoxMacAddress->setDisabled(true);
            ui->comboBoxProtocol->setDisabled(true);
        }
    }

    void KITHandWidget::on_pushButtonDisconnect_released()
    {
        _driver.disconnect();
        ui->pushButtonConnect->setEnabled(true);
        ui->pushButtonDisconnect->setDisabled(true);
        ui->widgetSensors->setDisabled(true);
        ui->widgetControl->setDisabled(true);

        ui->comboBoxMacAddress->setEnabled(true);
        ui->comboBoxProtocol->setEnabled(true);

        ui->labelStatus->setText("Disconnected");
    }

    void KITHandWidget::on_pushButtonIceConnect_released()
    {
        #ifdef KITHandWidget_WITH_ICE
        _server.startIceServer(
            ui->lineEditAdapterName->text().toStdString(),
            static_cast<std::uint64_t>(ui->spinBoxIcePort->value()),
            ui->lineEditIceIdentity->text().toStdString()
        );
        #endif
        ui->pushButtonIceConnect->setDisabled(true);
        ui->pushButtonIceDisconnect->setEnabled(true);
    }

    void KITHandWidget::on_pushButtonIceDisconnect_released()
    {
        #ifdef KITHandWidget_WITH_ICE
        _server.stopIceServer();
        #endif
        ui->pushButtonIceConnect->setEnabled(true);
        ui->pushButtonIceDisconnect->setDisabled(true);
    }

    void KITHandWidget::on_pushButtonSendThumb_released()
    {
        _driver.sendThumbPosition(
            static_cast<std::uint64_t>(ui->spinBoxThumbPos->value()),
            static_cast<std::uint64_t>(ui->spinBoxThumbVel->value()),
            static_cast<std::uint64_t>(ui->spinBoxThumbPWM->value())
        );
    }

    void KITHandWidget::on_pushButtonSendFingers_released()
    {
        _driver.sendFingersPosition(
            static_cast<std::uint64_t>(ui->spinBoxFingersPos->value()),
            static_cast<std::uint64_t>(ui->spinBoxFingersVel->value()),
            static_cast<std::uint64_t>(ui->spinBoxFingersPWM->value())
        );
    }

    void KITHandWidget::on_pushButtonSendGrasp_released()
    {
        _driver.sendGrasp(static_cast<std::uint64_t>(ui->comboBoxGrasp->currentIndex()));
    }

    void KITHandWidget::on_pushButtonSendRaw_released()
    {
        _driver.sendRaw(ui->lineEditCommandRaw->text().toStdString() + "\n");
    }
}
