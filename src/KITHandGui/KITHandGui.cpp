#ifdef KITHandGui_BUILD_ADVANCED_GUI
#include <KITHandAdvancedWidget.h>
#else
#include <KITHandWidget.h>
#endif

#include <QApplication>

#include "core/Logging.h"

#include <stdio.h>
#include <execinfo.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>

[[ noreturn ]] void handler(int sig) {
  void *array[10];
  int size;

  // get void*'s for all entries on the stack
  size = backtrace(array, 10);

  // print out all the frames to stderr
  fprintf(stderr, "Error: signal %d:\n", sig);
  backtrace_symbols_fd(array, size, STDERR_FILENO);
  exit(1);
}

using namespace KITHand;
int main(int argc, char *argv[])
{
    INIT_LOG

    signal(SIGSEGV, handler);

    QApplication a(argc, argv);
#ifdef KITHandGui_BUILD_ADVANCED_GUI
    KITHand::KITHandAdvancedWidget w;
#else
    KITHand::KITHandWidget w;
#endif
    w.show();

    return a.exec();
}
